﻿using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers
{
    public static class ResourceLockerModule
    {
        public static void AddResourceLockers(this IServiceCollection services)
        {
            services.AddSingleton<LockerResourceLocker>();
            services.AddSingleton<CustomerLocker>();
            services.AddSingleton<PreferenceLocker>();
            services.AddSingleton<PromoCodeLocker>();
            services.AddSingleton<ResourceLockers>();
            services.AddSingleton<ResourceLocker>();
        }
    }
}
