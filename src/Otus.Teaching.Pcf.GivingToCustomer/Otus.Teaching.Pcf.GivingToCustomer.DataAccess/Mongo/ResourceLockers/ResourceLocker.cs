﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers
{
    public class ResourceLocker
    {
        private readonly LockerResourceLocker _lockerResourceLocker;

        public ResourceLocker(LockerResourceLocker lockerResourceLocker)
        {
            _lockerResourceLocker = lockerResourceLocker;
        }

        public async Task WaitAsync(IEnumerable<IResourceLocker> lockers)
        {
            await _lockerResourceLocker.WaitAsync();

            try
            {
                foreach (var locker in lockers)
                {
                    await locker.WaitAsync();
                }
            }
            finally
            {
                _lockerResourceLocker.Release();
            }
        }

        public async Task ReleaseAsync(IEnumerable<IResourceLocker> lockers)
        {
            await _lockerResourceLocker.WaitAsync();

            try
            {
                foreach (var locker in lockers)
                {
                    locker.Release();
                }
            }
            finally
            {
                _lockerResourceLocker.Release();
            }
        }
    }
}
