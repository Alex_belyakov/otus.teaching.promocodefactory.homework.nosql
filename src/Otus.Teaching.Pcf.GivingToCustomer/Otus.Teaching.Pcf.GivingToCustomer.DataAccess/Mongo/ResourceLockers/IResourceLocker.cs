﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers
{
    public interface IResourceLocker
    {
        Task WaitAsync();
        void Release();
    }
}
