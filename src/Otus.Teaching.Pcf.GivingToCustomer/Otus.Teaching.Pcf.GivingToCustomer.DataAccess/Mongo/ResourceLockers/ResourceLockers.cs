﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers
{
    public class ResourceLockers
    {
        public readonly CustomerLocker CustomerLocker;
        public readonly PreferenceLocker PreferenceLocker;
        public readonly PromoCodeLocker PromoCodeLocker;

        public ResourceLockers(CustomerLocker customerLocker, PreferenceLocker preferenceLocker, PromoCodeLocker promoCodeLocker)
        {
            CustomerLocker = customerLocker;
            PreferenceLocker = preferenceLocker;
            PromoCodeLocker = promoCodeLocker;
        }
    }
}
