﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers
{
    public abstract class ResourceLockerBase : IResourceLocker
    {
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        public void Release()
        {
            _semaphore.Release();
        }

        public async Task WaitAsync()
        {
            await _semaphore.WaitAsync();
        }
    }
}
