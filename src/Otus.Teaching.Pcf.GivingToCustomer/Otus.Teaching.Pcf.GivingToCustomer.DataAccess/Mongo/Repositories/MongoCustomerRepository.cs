﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using MongoDB.Driver;
using MongoDB.Driver.Linq;

using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.Repositories
{
    public class MongoCustomerRepository : IMongoRepository<Customer>
    {
        private readonly IMongoCollection<Customer> _customerCollection;
        private readonly IMongoCollection<PromoCode> _promoCodeCollection;
        private readonly IMongoCollection<Preference> _preferenceCollection;
        private readonly ResourceLockers.ResourceLockers _lockers;
        private readonly ResourceLocker _locker;

        public MongoCustomerRepository(IMongoCollection<Customer> customerCollection, IMongoCollection<PromoCode> promoCodeCollection
            , IMongoCollection<Preference> preferenceCollection, ResourceLockers.ResourceLockers lockers, ResourceLocker locker)
        {
            _customerCollection = customerCollection;
            _promoCodeCollection = promoCodeCollection;
            _preferenceCollection = preferenceCollection;
            _locker = locker;
            _lockers = lockers;
        }

        public async Task<List<Customer>> GetAllAsync()
        {
            return await _customerCollection.AsQueryable().ToListAsync();
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            var findResult = await _customerCollection.FindAsync(t => t.Id == id);

            return await findResult.SingleOrDefaultAsync();
        }

        public async Task<List<Customer>> GetRangeByIdsAsync(IEnumerable<Guid> ids)
        {
            var customers = new List<Customer>();

            foreach (var id in ids)
            {
                var customer = await GetByIdAsync(id);
                customers.Add(customer);
            }

            return customers;
        }

        public Task<Customer> GetFirstWhere(Expression<Func<Customer, bool>> predicate)
        {
            return _customerCollection.AsQueryable().Where(predicate).FirstOrDefaultAsync();
        }

        public Task<List<Customer>> GetWhere(Expression<Func<Customer, bool>> predicate)
        {
            return _customerCollection.AsQueryable().Where(predicate).ToListAsync();
        }

        public async Task AddAsync(Customer entity)
        {
            var lockers = new IResourceLocker[] { _lockers.PreferenceLocker, _lockers.PromoCodeLocker, _lockers.CustomerLocker };

            await _locker.WaitAsync(lockers);

            try
            {
                await CheckPromoCodes(entity);
                await CheckPreferences(entity);

                await _customerCollection.InsertOneAsync(entity);
            }
            finally
            {
                await _locker.ReleaseAsync(lockers);
            }
        }

        public async Task UpdateAsync(Customer entity)
        {
            var lockers = new IResourceLocker[] { _lockers.PreferenceLocker, _lockers.PromoCodeLocker, _lockers.CustomerLocker };

            await _locker.WaitAsync(lockers);

            try
            {
                await CheckPromoCodes(entity);
                await CheckPreferences(entity);

                await _customerCollection.ReplaceOneAsync(t => t.Id == entity.Id, entity);
            }
            finally
            {
                await _locker.ReleaseAsync(lockers);
            }
        }

        public async Task DeleteAsync(Customer entity)
        {
            var lockers = new IResourceLocker[] { _lockers.CustomerLocker };

            await _locker.WaitAsync(lockers);

            try
            {
                await _customerCollection.DeleteOneAsync(t => t.Id == entity.Id);
            }
            finally
            {
                await _locker.ReleaseAsync(lockers);
            }
        }

        private async Task CheckPromoCodes(Customer entity)
        {
            var promoCodeIds = entity.PromoCodes?.Select(p => p.Id);

            if (promoCodeIds == null)
                return;

            foreach (var promoCodeId in promoCodeIds)
            {
                var promoCode = await _promoCodeCollection.FindAsync(p => p.Id == promoCodeId);

                if (promoCode == null)
                    throw new InvalidOperationException("Trying to refere to the not exisiting promocode object");
            }
        }

        private async Task CheckPreferences(Customer entity)
        {
            var preferenceIds = entity.Preferences.Select(p => p.Id);

            foreach(var preferenceId in preferenceIds)
            {
                var preference = await _preferenceCollection.FindAsync(p => p.Id == preferenceId);

                if (preference == null)
                    throw new InvalidOperationException("Trying to refere to the not exisiting preference object");
            }
        }        
    }
}
