﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.Repositories
{
    public class MongoPreferenceRepository : IMongoRepository<Preference>
    {
        private readonly IMongoCollection<Preference> _preferenceCollection;
        private readonly IMongoCollection<Customer> _customerCollection;
        private readonly IMongoCollection<PromoCode> _promoCodeCollection;
        private readonly ResourceLockers.ResourceLockers _lockers;
        private readonly ResourceLocker _locker;

        public MongoPreferenceRepository(IMongoCollection<Preference> preferenceCollection, IMongoCollection<Customer> customerCollection,
            IMongoCollection<PromoCode> promoCodeCollection, ResourceLockers.ResourceLockers lockers, ResourceLocker locker)
        {
            _locker = locker;
            _lockers = lockers;
            _promoCodeCollection = promoCodeCollection;
            _customerCollection = customerCollection;
            _preferenceCollection = preferenceCollection;
        }

        public async Task AddAsync(Preference entity)
        {
            await _locker.WaitAsync(new[] { _lockers.PreferenceLocker });

            try
            {
                await _preferenceCollection.InsertOneAsync(entity);
            }
            finally
            {
                await _locker.ReleaseAsync(new[] { _lockers.PreferenceLocker });
            }
        }

        public async Task DeleteAsync(Preference entity)
        {
            var lockers = new IResourceLocker[] { _lockers.PreferenceLocker, _lockers.PromoCodeLocker, _lockers.CustomerLocker };

            await _locker.WaitAsync(lockers);

            try
            {
                await DeleteCustomerPreferences(entity);
                await DeletePromoCodePreferences(entity);
                await _preferenceCollection.DeleteOneAsync(p => p.Id == entity.Id);
            }
            finally
            {
                await _locker.ReleaseAsync(lockers);
            }
        }

        public async Task UpdateAsync(Preference entity)
        {
            var lockers = new IResourceLocker[] { _lockers.PreferenceLocker, _lockers.PromoCodeLocker, _lockers.CustomerLocker };

            await _locker.WaitAsync(lockers);

            try
            {
                await UpdateCustomerPreferences(entity);
                await UpdatePromoCodePreferences(entity);
                await _preferenceCollection.ReplaceOneAsync(p => p.Id == entity.Id, entity);
            }
            finally
            {
                await _locker.ReleaseAsync(lockers);
            }
        }

        public Task<List<Preference>> GetAllAsync()
        {
            return _preferenceCollection.AsQueryable().ToListAsync();
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            var findResult = await _preferenceCollection.FindAsync(t => t.Id == id);
            var listResult = await findResult.ToListAsync();

            return listResult.SingleOrDefault();
        }

        public Task<Preference> GetFirstWhere(Expression<Func<Preference, bool>> predicate)
        {
            return _preferenceCollection.AsQueryable().Where(predicate).FirstOrDefaultAsync();
        }

        public Task<List<Preference>> GetWhere(Expression<Func<Preference, bool>> predicate)
        {
            return _preferenceCollection.AsQueryable().Where(predicate).ToListAsync();
        }

        public async Task<List<Preference>> GetRangeByIdsAsync(IEnumerable<Guid> ids)
        {

            var preferences = new List<Preference>();

            foreach (var id in ids)
            {
                var preference = await GetByIdAsync(id);
                preferences.Add(preference);
            }

            return preferences;
        }


        private async Task UpdateCustomerPreferences(Preference entity)
        {
            var cursor = await _customerCollection.FindAsync(c => c.Preferences.Any(p => p.Id == entity.Id));
            var customers = await cursor.ToListAsync();

            foreach (var customer in customers)
            {
                customer.Preferences.RemoveAll(p => p.Id == entity.Id);
                customer.Preferences.Add(entity);
                await _customerCollection.ReplaceOneAsync(c => c.Id == customer.Id, customer);
            }
        }

        private async Task UpdatePromoCodePreferences(Preference entity)
        {
            var update = Builders<PromoCode>.Update
                .Set(p => p.Preference, entity);

            await _promoCodeCollection.UpdateManyAsync(p => p.Preference.Id == entity.Id, update);
        }

        private async Task DeleteCustomerPreferences(Preference entity)
        {
            var cursor = await _customerCollection.FindAsync(c => c.Preferences.Any(p => p.Id == entity.Id));
            var customers = await cursor.ToListAsync();

            foreach (var customer in customers)
            {
                customer.Preferences.RemoveAll(p => p.Id == entity.Id);
                await _customerCollection.ReplaceOneAsync(c => c.Id == customer.Id, customer);
            }
        }

        private async Task DeletePromoCodePreferences(Preference entity)
        {
            var update = Builders<PromoCode>.Update
                .Set(p => p.Preference, null);

            await _promoCodeCollection.UpdateManyAsync(p => p.Preference.Id == entity.Id, update);
        }
    }
}
