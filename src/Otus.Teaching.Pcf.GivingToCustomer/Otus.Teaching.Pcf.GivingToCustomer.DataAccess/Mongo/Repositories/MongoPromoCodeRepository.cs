﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using MongoDB.Driver;
using MongoDB.Driver.Linq;

using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.Repositories
{
    public class MongoPromoCodeRepository : IMongoRepository<PromoCode>
    {
        private readonly IMongoCollection<PromoCode> _promoCodeCollection;
        private readonly IMongoCollection<Customer> _customerCollection;
        private readonly IMongoCollection<Preference> _preferenceCollection;
        private readonly ResourceLockers.ResourceLockers _lockers;
        private readonly ResourceLocker _locker;

        public MongoPromoCodeRepository(IMongoCollection<PromoCode> promoCodeCollection, IMongoCollection<Customer> customerCollection,
            IMongoCollection<Preference> preferenceCollection, ResourceLockers.ResourceLockers lockers, ResourceLocker locker)
        {
            _locker = locker;
            _lockers = lockers;
            _preferenceCollection = preferenceCollection;
            _promoCodeCollection = promoCodeCollection;
            _customerCollection = customerCollection;
        }

        public async Task AddAsync(PromoCode entity)
        {

            var lockers = new IResourceLocker[] { _lockers.PreferenceLocker, _lockers.PromoCodeLocker };

            await _locker.WaitAsync(lockers);

            try
            {
                await CheckPreferences(entity);
                await _promoCodeCollection.InsertOneAsync(entity);
            }
            finally
            {
                await _locker.ReleaseAsync(lockers);
            }
        }

        public async Task DeleteAsync(PromoCode entity)
        {

            var lockers = new IResourceLocker[] { _lockers.PromoCodeLocker, _lockers.CustomerLocker };

            await _locker.WaitAsync(lockers);

            try
            {
                await DeleteCustomerPromoCodes(entity);
                await _promoCodeCollection.DeleteOneAsync(p => p.Id == entity.Id);
            }
            finally
            {
                await _locker.ReleaseAsync(lockers);
            }
        }

        public async Task UpdateAsync(PromoCode entity)
        {

            var lockers = new IResourceLocker[] { _lockers.PreferenceLocker, _lockers.PromoCodeLocker, _lockers.CustomerLocker };

            await _locker.WaitAsync(lockers);

            try
            {
                await CheckPreferences(entity);
                await UpdateCustomerPromoCodes(entity);
                await _promoCodeCollection.ReplaceOneAsync(p=>p.Id == entity.Id, entity);
            }
            finally
            {
                await _locker.ReleaseAsync(lockers);
            }
        }

        public Task<List<PromoCode>> GetAllAsync()
        {
            return _promoCodeCollection.AsQueryable().ToListAsync();
        }

        public async Task<PromoCode> GetByIdAsync(Guid id)
        {
            var cursor = await _promoCodeCollection.FindAsync(p => p.Id == id);

            return await cursor.FirstOrDefaultAsync();
        }

        public Task<PromoCode> GetFirstWhere(Expression<Func<PromoCode, bool>> predicate)
        {
            return _promoCodeCollection.AsQueryable().Where(predicate).FirstOrDefaultAsync();
        }

        public async Task<List<PromoCode>> GetRangeByIdsAsync(IEnumerable<Guid> ids)
        {
            var promoCodes = new List<PromoCode>();

            foreach (var id in ids)
            {
                var promoCode = await GetByIdAsync(id);
                promoCodes.Add(promoCode);
            }

            return promoCodes;
        }

        public Task<List<PromoCode>> GetWhere(Expression<Func<PromoCode, bool>> predicate)
        {
            return _promoCodeCollection.AsQueryable().Where(predicate).ToListAsync();
        }

        private async Task UpdateCustomerPromoCodes(PromoCode entity)
        {
            var cursor = await _customerCollection.FindAsync(c => c.PromoCodes.Any(p => p.Id == entity.Id));
            var customers = await cursor.ToListAsync();

            foreach (var customer in customers)
            {
                customer.PromoCodes.RemoveAll(p => p.Id == entity.Id);
                customer.PromoCodes.Add(entity);
                await _customerCollection.ReplaceOneAsync(c => c.Id == customer.Id, customer);
            }
        }

        private async Task DeleteCustomerPromoCodes(PromoCode entity)
        {
            var cursor = await _customerCollection.FindAsync(c => c.PromoCodes.Any(p => p.Id == entity.Id));
            var customers = await cursor.ToListAsync();

            foreach (var customer in customers)
            {
                customer.PromoCodes.RemoveAll(p => p.Id == entity.Id);
                await _customerCollection.ReplaceOneAsync(c => c.Id == customer.Id, customer);
            }
        }

        private async Task CheckPreferences(PromoCode entity)
        {
            var preferenceId = entity.Preference.Id;

            var preference = await _preferenceCollection.FindAsync(p => p.Id == preferenceId);

            if (preference == null)
                throw new InvalidOperationException("Trying to refere to the not exisiting preference object");
        }
    }
}
