﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo
{
    public static class MongoDbReposModule
    {
        public static void AddMongoDbRepos(this IServiceCollection services)
        {
            services.AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<Customer>(nameof(Customer)));
            services.AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<PromoCode>(nameof(PromoCode)));
            services.AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<Preference>(nameof(Preference)));

            services.AddResourceLockers();

            services.AddScoped<IMongoRepository<Customer>, MongoCustomerRepository>();
            services.AddScoped<IMongoRepository<PromoCode>, MongoPromoCodeRepository>();
            services.AddScoped<IMongoRepository<Preference>, MongoPreferenceRepository>();
        }
    }
}
