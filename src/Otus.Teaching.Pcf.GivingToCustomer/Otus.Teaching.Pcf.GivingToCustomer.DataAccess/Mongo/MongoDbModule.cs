﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo
{
    public static class MongoDbModule
    {
        public static void AddMongoDb(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddSingleton<IMongoClient>(_ =>
                    new MongoClient(configuration["MongoDbCustomers:ConnectionString"]))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .GetDatabase(configuration["MongoDbCustomers:Database"]));
        }
    }
}
