﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}