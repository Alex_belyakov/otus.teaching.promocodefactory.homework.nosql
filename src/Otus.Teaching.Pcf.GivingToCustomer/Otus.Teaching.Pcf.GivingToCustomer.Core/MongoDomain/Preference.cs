﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
    }
}