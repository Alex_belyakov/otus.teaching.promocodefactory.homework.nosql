﻿using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain
{
    public class Customer : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public virtual List<Preference> Preferences { get; set; }

        public virtual List<PromoCode> PromoCodes { get; set; }
    }
}