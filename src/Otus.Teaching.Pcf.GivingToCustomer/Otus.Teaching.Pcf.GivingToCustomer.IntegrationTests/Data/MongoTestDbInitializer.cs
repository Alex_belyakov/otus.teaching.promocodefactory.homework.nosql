﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data
{
    public class MongoTestDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _mongoDb;

        public MongoTestDbInitializer(IMongoDatabase mongoDb)
        {
            _mongoDb = mongoDb;
        }

        public void InitializeDb()
        {
            InitializeDbAsync().Wait();
        }

        public async Task InitializeDbAsync()
        {
            await ClearCollectionsAsync();
            await CreateCollectionsAsync();
            await FillPreferences();
            await FillCustomers();
        }

        public void CleanDb()
        {
            ClearCollectionsAsync().Wait();
        }

        private async Task FillCustomers()
        {
            var customers = _mongoDb.GetCollection<Customer>(nameof(Customer));

            await customers.InsertManyAsync(FakeDataFactory.Customers);
        }

        private async Task FillPreferences()
        {
            var preferences = _mongoDb.GetCollection<Preference>(nameof(Preference));
            var preferenceBuilder = Builders<Preference>.IndexKeys;
            var preferenceIndexModel = new CreateIndexModel<Preference>(preferenceBuilder.Ascending(pb => pb.Id));

            await preferences.Indexes.CreateOneAsync(preferenceIndexModel);
            await preferences.InsertManyAsync(FakeDataFactory.Preferences);
        }

        private async Task ClearCollectionsAsync()
        {
            await _mongoDb.DropCollectionAsync(nameof(Preference));
            await _mongoDb.DropCollectionAsync(nameof(PromoCode));
            await _mongoDb.DropCollectionAsync(nameof(Customer));
        }

        private async Task CreateCollectionsAsync()
        {
            await _mongoDb.CreateCollectionAsync(nameof(Preference));
            await _mongoDb.CreateCollectionAsync(nameof(PromoCode));
            await _mongoDb.CreateCollectionAsync(nameof(Customer));
        }
    }
}