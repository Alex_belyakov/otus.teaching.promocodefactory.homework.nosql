﻿using System;
using System.Threading.Tasks;

using MongoDB.Driver;

using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.ResourceLockers;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Verifiers;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class MongoDatabaseFixture: IDisposable
    {
        private readonly MongoTestDbInitializer _efTestDbInitializer;
        
        public MongoDatabaseFixture()
        {
            var mongoClient = new MongoClient(@"mongodb://localhost");
            var mongoDb = mongoClient.GetDatabase("MongoDbCustomers:Database");

            var customerCollection = mongoDb.GetCollection<Customer>(nameof(Customer));
            var preferenceCollection = mongoDb.GetCollection<Preference>(nameof(Preference));
            var promoCodeCollection = mongoDb.GetCollection<PromoCode>(nameof(PromoCode));

            var customerLocker = new CustomerLocker();
            var preferenceLocker = new PreferenceLocker();
            var promoCodeLocker = new PromoCodeLocker();
            var lockerLocker = new LockerResourceLocker();
            var lockers = new ResourceLockers(customerLocker, preferenceLocker, promoCodeLocker);
            var locker = new ResourceLocker(lockerLocker);

            PreferenceRepository = new MongoPreferenceRepository(preferenceCollection, customerCollection, promoCodeCollection, lockers, locker);
            CustomerRepository = new MongoCustomerRepository(customerCollection, promoCodeCollection, preferenceCollection, lockers, locker);
            CustomerVerifier = new CreateOrEditCustomerVerifier(PreferenceRepository);

            _efTestDbInitializer = new MongoTestDbInitializer(mongoDb);
            _efTestDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _efTestDbInitializer.CleanDb();
        }

        public IMongoRepository<Preference> PreferenceRepository { get; private set; }
        public IMongoRepository<Customer> CustomerRepository { get; private set; }
        public CreateOrEditCustomerVerifier CustomerVerifier { get; private set; }
    }
}