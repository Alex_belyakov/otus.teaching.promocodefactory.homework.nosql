﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.MongoDomain;

using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Verifiers
{
    public class CreateOrEditCustomerVerifier
    {
        private readonly IMongoRepository<Preference> _preferenceRepository;

        public CreateOrEditCustomerVerifier(IMongoRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        public async Task<bool> VerifyAsync(CreateOrEditCustomerRequest request)
        {
            foreach (var preferenceId in request.PreferenceIds)
            {
                var preference = await _preferenceRepository.GetByIdAsync(preferenceId);
                
                if (preference == null)
                    return false;
            }

            return true;
        }
    }
}
